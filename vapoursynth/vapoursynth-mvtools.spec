#
# spec file for package vapoursynth-mvtools
#
# Copyright (c) 2016 SUSE LINUX GmbH, Nuernberg, Germany.
#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.

# Please submit bugfixes or comments via http://bugs.opensuse.org/
#

Name:           vapoursynth-mvtools
Version:	r23
Release:	1.24
License:	GPL-2.0
Summary:	A set of filters for motion estimation and compensation for VapourSynth
Url:		https://github.com/dubhater/vapoursynth-mvtools
Group:		Productivity/Graphics/Bitmap Editors
Source:		https://github.com/dubhater/vapoursynth-mvtools/archive/v23.tar.gz
BuildRequires:	gcc-c++ yasm pkg-config automake autoconf libtool
BuildRequires:	vapoursynth-devel >= R30 fftw3-devel >= 3.3.4 nasm
BuildRoot:      %{_tmppath}/%{name}-%{version}-build

%description
MVTools is a set of filters for motion estimation and compensation.

This is a port of version 2.5.11.3 of the Avisynth plugin (the latest from http://avisynth.org.ru/mvtools/mvtools2.html).
Some changes from version 2.5.11.9 of the SVP fork have been incorporated as well (http://www.svp-team.com/wiki/Download).

%prep
%setup -q -n vapoursynth-mvtools-23

%build
autoreconf -fi
%configure --libdir="%{_libdir}/vapoursynth"
make %{?_smp_mflags}

%install
make install DESTDIR=%{buildroot} %{?_smp_mflags}

%post

%postun

%files
%defattr(-,root,root)
%doc readme.rst
%{_libdir}/vapoursynth/libmvtools*

